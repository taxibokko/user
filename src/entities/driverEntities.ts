export class DriverEntity{

    
    isConnected: boolean; // boolean to check whether the user is connected or not

    constructor(private _firstName?: string, private _lastName?: string,  private _phone?: number, private _uuid?: string){
        
    }

//---------------------------------------------SETTER-----------------------------------------
    set firstName(firstNameGiven: string){
        this._firstName = firstNameGiven;
    }
    
    set lastName(lastNameGiven: string){
        this._lastName = lastNameGiven;
    }

    set phone(phoneGiven: number){
        this._phone = phoneGiven;
    }

    set uuid (uuid:string){
        this._uuid = uuid;
    }

//---------------------------------------------GETTER-----------------------------------------
    get firstName (): string{
        return this._firstName;
    }
    
    get lastName(): string {
        return this._lastName;
    }

    get phone(): number {
        return this._phone;
    }

    get uuid (): string {
        return this._uuid;
    }

}

//---------------------------------------------DriverLicenseEntity-----------------------------------------
export class DriverLicenseEntity{
    driverLicenceNumber: string;
    vehiculeType: string;
    dateIssuedOn: Date;
    dateExpiration: Date;
    constructor(){

    }
}