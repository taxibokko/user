import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodeVerificationPage } from './code-verification';

@NgModule({
  declarations: [
    CodeVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(CodeVerificationPage),
  ],
})
export class CodeVerificationPageModule {}
