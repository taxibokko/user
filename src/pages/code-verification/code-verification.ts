import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Register } from '../register/register';
import * as firebase from 'firebase';


/**
 * Generated class for the CodeVerificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-code-verification',
  templateUrl: 'code-verification.html',
})
export class CodeVerificationPage {
  private telephone:string;
  private indicateur :string = "+221";
  private confirmationData:any;
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams) {

    console.log(this.navParams.get("phone"));
    this.telephone = this.indicateur+this.navParams.get("phone");
    this.confirmationData= this.navParams.get("confirmation");



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CodeVerificationPage');
    this.presentAlert();
  }
  backButtonClick(){
    this.navCtrl.pop();
  }
  //Fonction pour confirmation de code
  confirmeCode(code:string):void{
    let self=this;
    self.confirmationData.confirm(code)
    .then(function (result) {
      // code valide
      //recueillir les infos du profil
     self.navCtrl.push(Register,{phone:self.navParams.get("phone")})

    }).catch(function (error) {
      // code incorrecte
      self.alertCodeInvalide();
      // ...
    });

  }


  private presentAlert():void {
   let self = this;
    let toast = self.toastCtrl.create({
      message: 'Renseignez le code reçu au '+self.telephone+'!',
      duration: 5000,
      position: 'top'
  });
  toast.present();
  }

 private alertCodeInvalide():void{
   let self =this;
  let toast = self.toastCtrl.create({
    message: 'le code est invalide !! Veiller réessayer',
    duration: 4000,
    position: 'center',
});
toast.present();
  }

  public renvoyerCode():void{

    let self = this;
     var appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha', {
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
      }
    });

    firebase.auth().signInWithPhoneNumber(self.telephone, appVerifier)
    .then( confirmationResult => {
      console.log(self.confirmationData);
      self.confirmationData =  confirmationResult;
      console.log(self.confirmationData);
      self.AlertRenvoie();

    });

  }

  private AlertRenvoie():void {
    let self = this;
     let toast = self.toastCtrl.create({
       message: 'Le code vous a été renvoyé au '+self.telephone+' !',
       duration: 2000,
       position: 'center'
   });
   toast.present();
   }
}
