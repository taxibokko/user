import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Global } from '../../providers/global';
import { BokkoEntity } from '../../entities/bokkoEntity';
import { stringify } from '@angular/compiler/src/util';


@IonicPage()
@Component({
  selector: 'page-destination-modal',
  templateUrl: 'destination-modal.html',
})
export class DestinationModal {

  bokko1: BokkoEntity;
  bokko2: BokkoEntity;
  bokko3: BokkoEntity;
  allBokkos: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public global: Global) {
    this.allBokkos = [];
    this.bokko1 = new BokkoEntity();
    this.bokko2 = new BokkoEntity();
    this.bokko3 = new BokkoEntity();
    this.seedBokkos();
  }

 // Active Accept Trip function
 AcceptTrip(){
    console.log(this.global.accept)
    this.global.accept = true;
  }

  // close Modal
  dismiss() {
    this.viewCtrl.dismiss();
  }
  seedBokkos(){
    this.bokko1.start = "Parcelles Assainies";
    this.bokko1.end = "Dakar Plateau";
    this.bokko1.driver.firstName = "Omar Diop";
    this.bokko1.clients = "2";
    this.bokko1.cost = "950";
    this.bokko1.time = "8";
  
    this.bokko2.start = "Foire CICES";
    this.bokko2.end = "Dakar Plateau";
    this.bokko2.driver.firstName = "Assane Ngom";
    this.bokko2.clients = "3";
    this.bokko2.cost = "1000";
    this.bokko2.time = "15";
  
    this.bokko3.start = "Cité Keur Damel";
    this.bokko3.end = "Dakar Plateau";
    this.bokko3.driver.firstName = "Amadou Diallo";
    this.bokko3.clients = "1";
    this.bokko3.cost = "1250";
    this.bokko3.time = "20";
    
    this.allBokkos = [this.bokko1,this.bokko2, this.bokko3];
  
  }
}