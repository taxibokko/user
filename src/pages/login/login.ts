
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import * as firebase from 'firebase';
import { DataProvider } from '../../providers/data/data';
import { CodeVerificationPage } from '../code-verification/code-verification';
import { ConditionsUtilisationPage } from '../conditions-utilisation/conditions-utilisation';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class Login {
  verificationId: any;
  code: String = "";
  appVerifier:firebase.auth.RecaptchaVerifier;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public authProvider:AuthProvider, public loadingCtrl:LoadingController, public data:DataProvider, public toastCtrl:ToastController
    ) {}

    getVerificationCode(numero:string):void{
       let self = this;
       self.appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
        'callback': function(response) {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
        }
      });

      firebase.auth().signInWithPhoneNumber("+221"+numero, null)
      .then( confirmationResult => {
      this.navCtrl.push(CodeVerificationPage,{phone :numero,confirmation:confirmationResult});

      });
    }

backButtonClick(){
  this.navCtrl.pop();
}

public conditionsUtilisation():void{
  this.navCtrl.push(ConditionsUtilisationPage);
}
}

