import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Login } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class Setting {

  constructor(public navCtrl: NavController, public navParams: NavParams,public actionSheetCtrl: ActionSheetController, public authService:AuthProvider) {
  }

  presentEmergency() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Emergency Contacts',
      buttons: [
        {
          text: 'Driver',
          role: 'Driver',
          handler: () => {
            console.log('Driver clicked');
          }
        },{
          text: 'Technical support',
          handler: () => {
            console.log('Technical clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
public logout():void{
  let self = this;
  self.authService.signOut()
  .then(snapchot=>{
    this.navCtrl.setRoot(Login);
  });
}
}
