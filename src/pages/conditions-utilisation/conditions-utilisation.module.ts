import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConditionsUtilisationPage } from './conditions-utilisation';

@NgModule({
  declarations: [
    ConditionsUtilisationPage,
  ],
  imports: [
    IonicPageModule.forChild(ConditionsUtilisationPage),
  ],
})
export class ConditionsUtilisationPageModule {}
