import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import $ from "jquery";
import 'intl-tel-input';
import { RestProvider } from '../../providers/rest/rest';
import { DriverEntity as ClientEntity } from '../../entities/driverEntities';
import { AuthProvider } from '../../providers/auth/auth';



// ------------------------------------------DECORATORS----------------------------------------------------
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

// ------------------------------------------REGISTER CLASS----------------------------------------------------
export class Register {
// ------------------------------------------PROPERTIES----------------------------------------------------
  clientUser = new ClientEntity();
  driverInfo: any;
  // informationPage: 'Information';
  public telephone:string;


// ------------------------------------------CONSTRUCTOR----------------------------------------------------
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController, public toastCtrl: ToastController, public authService:AuthProvider, public navParams: NavParams, public restProvider: RestProvider) {
    var emptyField = "";
    this.clientUser.firstName = emptyField;
    this.clientUser.lastName = emptyField;
    this.clientUser.uuid = emptyField;
    this.clientUser.phone = this.telephone = this.navParams.get("phone");

  }
// ------------------------------------------METHOD: backButtonClick()----------------------------------------------------
// back function
  backButtonClick(){
    this.navCtrl.pop();
  }

// ------------------------------------------METHOD: ngOnInit()----------------------------------------------------
// intlTelInput for select country id
  ngOnInit(): any {
    let telInput = $("#phone");

    telInput.intlTelInput();
    // listen to "keyup", but also "change" to update when the user selects a country
    telInput.on("keyup change", function() {
      // var intlNumber = telInput.intlTelInput("getNumber");
    });
  }

// ------------------------------------------METHOD: saveDriverCredentials()----------------------------------------------------
  saveDriverCredentials(){
    let self = this;
    self.driverInfo = {
      "Prénoms": this.clientUser.firstName,
      "Nom": this.clientUser.lastName,
     //"MotDePasse" : this.clientUser.uuid,
      "telephone" : "+221"+this.clientUser.phone
    };
    let loader = this.loadingCtrl.create({
      content: 'Enregistrement...',
      dismissOnPageChange: true
  });
  loader.present();
  self.authService.addUser(self.driverInfo, self.authService.getLoggedInUser().uid);
  loader.dismiss()
      .then(() => {
              let toast = self.toastCtrl.create({
                  message: 'Informations enregistrées avec Succès',
                  duration: 4000,
                  position: 'top'
              });
              toast.present();
  self.goToHomePage();
}).catch(function (error) {
  var errorMessage = error.message;
  console.error(error);
  loader.dismiss().then(() => {
      let toast = self.toastCtrl.create({
          message: errorMessage,
          duration: 4000,
          position: 'top'
      });
      toast.present();
  });
});
    // console.log(this.driverInfo);
  //  this.restProvider.postDriverInfo(this.driverInfo);
   // this.navCtrl.push('Home');
  }
private goToHomePage():void{
 let self = this;
 self.navCtrl.setRoot('Home');
}

}
