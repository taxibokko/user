import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';


@Injectable()
export class AuthProvider {
  usersRef: any = firebase.database().ref('users');

  constructor(public http: HttpClient) {
    console.log('Hello AuthProvider Provider');
  }

  signInUser(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }



signOut() {
    return firebase.auth().signOut();
}

addUser(userInfos:any, uid: string) {
  console.log(uid);
  console.log(this.usersRef);
  console.log(userInfos);
  this.usersRef.child(uid).update(userInfos);

}


getLoggedInUser() {
    return firebase.auth().currentUser;
}
onAuthStateChanged(callback) {
    return firebase.auth().onAuthStateChanged(callback);
}
}

