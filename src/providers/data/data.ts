import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
  usersRef: any = firebase.database().ref('users');
  public userPhoneNumber:any;

  constructor(public http: HttpClient) {
    console.log('Hello DataProvider Provider');
  }

  getUser(userUid: string) {
    return this.usersRef.child(userUid).once('value');
}



}
