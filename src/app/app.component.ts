import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';
import { Login } from '../pages/login/login';
import { DataProvider } from '../providers/data/data';


//--------------------------------------------Firebase object Configuration-----------------------------------------------
firebase.initializeApp({
  apiKey: 'AIzaSyC1u-qjosXUSZY5EWPz-jmab2T46Yf7Y2s',
  authDomain: "taxibokko.firebaseapp.com",
  databaseURL: "https://taxibokko.firebaseio.com",
  projectId: 'taxibokko',
  storageBucket: "taxibokko.appspot.com",
  messagingSenderId: "2693849356",
  appId: "1:2693849356:web:5351bf8a865d0efa"
}); // Initialize firebase


//--------------------------------------------------------DECORATOR-----------------------------------------------
@Component({
  templateUrl: 'app.html'
})

//--------------------------------------------------------MY APP CLASS----------------------------------------------
export class MyApp {
//--------------------------------------------------------DOM PROPERTY----------------------------------------------
  @ViewChild(Nav) nav: Nav;
//--------------------------------------------------------PROPERTIES-----------------------------------------------
  rootPage: any ;
  pages: Array<{title: string, component: any}>;

  //--------------------------------------------------------CONSTRUCTOR-----------------------------------------------
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public dataService: DataProvider) {

    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'Home' },
      { title: 'List', component: 'Login' }
    ];

    this.initdatas();

  }

//--------------------------------------------------------INITIALIZE APP METHOD-----------------------------------------------
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

  }
//--------------------------------------------------------OPEN PAGE METHOD-----------------------------------------------

  openPage(page: any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  initdatas(){
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (!user) {
       this.rootPage = 'Home';
        unsubscribe();
      } else {
       this.dataService.getUser(user.uid)
       .then((snapshot)=>{
       let userData=snapshot.val(); // information de l4 utilisateur
       console.log(userData);
      // this.profile=userdatas;
      });
     this.rootPage = 'Home';
        unsubscribe();
      }
    });
  }
}
