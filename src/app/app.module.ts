import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { Geolocation } from '@ionic-native/geolocation';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../providers/rest/rest';
import { HttpClientModule } from '@angular/common/http';
import { Device } from '@ionic-native/device';
import { Global } from '../providers/global';
import { AuthProvider } from '../providers/auth/auth';
import { DataProvider } from '../providers/data/data';
import { CodeVerificationPage } from '../pages/code-verification/code-verification';
import { HomePageModule } from '../pages/home/home.module';
import { Register } from '../pages/register/register';
import { RegisterModule } from '../pages/register/register.module';
import { Login } from '../pages/login/login';
import { LoginPageModule } from '../pages/login/login.module';
import { ConditionsUtilisationPage } from '../pages/conditions-utilisation/conditions-utilisation';

var config = {
  backButtonText: '',
  backButtonIcon: 'md-arrow-back',
  pageTransition: 'md',
  mode:'md'
};

@NgModule({
  declarations: [
    MyApp,
    CodeVerificationPage,
    Register,
    ConditionsUtilisationPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,config),
    HomePageModule,
    LoginPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CodeVerificationPage,
    Register,
    ConditionsUtilisationPage


  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    RestProvider,
    Global,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Device,
    AuthProvider,
    DataProvider
  ]
})
export class AppModule {}
