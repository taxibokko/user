"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const Geofire = require("geofire");
const node_fetch_1 = require("node-fetch");
const crypto = require("crypto");
const payExpressUrl = 'https://payexpresse.com/api/payment/request-payment';
const MY_PAYEXPRESSE_API_KEY = '2e3586cc01310450806c077979d205d9f4b71809b996fc3789eb12180815e8fa';
const MY_PAYEXPRESSE_API_SECRET = '6759d2a0e671f2429d2154845fa44466d03d56e5a5e7657cdf20e852855050b2';
admin.initializeApp(functions.config().firebase);
const geo = Geofire;
const geofireSoloDrivers = new geo(admin.database().ref(`drivers/geolocations/`));
const geofireBokkoDrivers = new geo(admin.database().ref(`drivers_bokkos/geolocations/`));
let geofireQuerySoloDriver;
let geofireQueryBokkoDriver;
/*******************************************************************************
            NOTIFY CLIENT WHEN THE DRIVER ACCEPTS THE TRIP
                              
 *******************************************************************************/
//NOTIFY THE CLIENT WHEN THE DRIVER ACCEPTS THE TRIP
exports.notifyTaxiArrivalToClient = functions.database.ref('clients/credentials/{client_uid}/')
    .onWrite(async (snapshot, context) => {
    // if the snapshot exists
    if (snapshot.after.exists()) {
        const client_status = snapshot.after.val().status;
        const client_uid = snapshot.after.key;
        console.log('CLIENT UID: ', client_uid);
        await admin.database().ref(`clients/tokens/${client_uid}`).once('value', (token) => {
            const client_token = token.val();
            const options = {
                priority: "high",
                timeToLive: 60 * 5
            };
            let notifBody;
            if (client_status === 'client_in_taxi') {
                const clientInTaxiNotif = {
                    notification: {
                        title: 'TaxiBokko',
                        body: `\nVotre chauffeur est arrivé \n`,
                        sound: 'default',
                        badge: '1',
                        click_action: 'FCM_PLUGIN_ACTIVITY'
                    },
                    data: {
                        title: 'Taxi',
                        status: client_status,
                        timestamp: context.timestamp
                    }
                };
                notifBody = clientInTaxiNotif;
            }
            else if (client_status === 'client_dropped_off') {
                const clientDroppedNotif = {
                    notification: {
                        title: '   TaxiBokko',
                        body: `\nÊtes-vous arrivé(e) á destination ?!\n`,
                        sound: 'default',
                        click_action: 'FCM_PLUGIN_ACTIVITY'
                    },
                    data: {
                        title: 'Taxi',
                        status: client_status,
                    }
                };
                notifBody = clientDroppedNotif;
            }
            admin.messaging().sendToTopic(client_token, notifBody, options)
                .then((_) => {
                console.log('CLIENT NOTIFIED', client_uid, client_status);
            }).catch((error) => console.log('ERROR NOTIFYING CLIENT', error));
            admin.database().ref(`clients/notifications/${client_uid}/`).set(notifBody)
                .then(() => console.log('NOTIFICATION SENT TO FIREBASE DATABASE'))
                .catch((er) => console.log('ERROR SENDING NOTIFICATION TO FIREBASE DATABASE: ', er));
        });
    }
});
/*******************************************************************************
                              NEW TRIP COST
                              
 *******************************************************************************/
exports.newtripCost = functions.database.ref('clients/status/{client_uid}/')
    .onWrite(async (snapshot, context) => {
    // if the snapshot exists
    if (snapshot.after.exists()) {
        if (snapshot.after.val().driver_answer == 'client_paid') {
            return;
        }
        const trip_cost = snapshot.after.val().new_trip_cost;
        const client_uid = snapshot.after.key;
        console.log('CLIENT UID: ', client_uid);
        await admin.database().ref(`clients/tokens/${client_uid}`).once('value', (token) => {
            const client_token = token.val();
            const options = {
                priority: "high",
                timeToLive: 60 * 5
            };
            let notifBody;
            if (trip_cost !== null && trip_cost !== snapshot.before.val().new_trip_cost) {
                const newTripCostNotif = {
                    notification: {
                        title: 'TaxiBokko',
                        body: `\nLe coût de votre course a été recalculé\n`,
                        sound: 'default',
                        click_action: 'FCM_PLUGIN_ACTIVITY'
                    },
                    data: {
                        title: 'new_trip_cost',
                        status: 'client_still_in_taxi',
                        new_trip_cost: trip_cost.toString(),
                    }
                };
                notifBody = newTripCostNotif;
                admin.messaging().sendToTopic(client_token, notifBody, options)
                    .then((_) => {
                    console.log('CLIENT NOTIFIED', client_uid, trip_cost, context.timestamp);
                }).catch((error) => console.log('ERROR NOTIFYING CLIENT', error));
                admin.database().ref(`clients/notifications/${client_uid}/`).set(notifBody)
                    .then(() => console.log("NOTIFICATION SENT TO FIREBASE"))
                    .catch((er) => console.log("ERROR SENDING NOTIFICATION TO FIREBASE: ", er));
            }
        });
    }
});
/*******************************************************************************
                              GET NEARBY DRIVERS
                              
 *******************************************************************************/
exports.getNearbyDrivers = functions.database.ref(`clients/geolocations/{client_uid}/`)
    .onCreate((snapshot) => {
    const client_geo_data = snapshot.val();
    const client_key = snapshot.key;
    // SET THE SOLO DRIVER GEOLOCATION INFORMATION UNDER NEARBY DRIVERS NODE FOR THE CLIENT
    geofireQuerySoloDriver = geofireSoloDrivers.query({
        center: [client_geo_data.l[0], client_geo_data.l[1]],
        radius: 50
    });
    geofireQuerySoloDriver.on('key_entered', (key, location, distance) => {
        console.log("NEW HIT => DRIVER FOUND: ", key);
        admin.database().ref(`clients/nearby_drivers/${client_key}/${key}`).set({
            type: 'solo',
            distance: distance,
            location: location
        })
            .then(() => console.log('driver location info sent under clients_nearby_drivers node'))
            .catch(er => console.log('error sending driver locatio info under clients nearby_drivers node: ', er));
    });
    // SET THE SOLO DRIVER GEOLOCATION INFORMATION UNDER NEARBY DRIVERS NODE FOR THE CLIENT
    geofireQuerySoloDriver.on('key_exited', (key, location, distance) => {
        console.log("NEW HIT => DRIVER EXITED: ", key, location, distance);
        admin.database().ref(`clients/nearby_drivers/${client_key}/${key}`)
            .remove()
            .then(() => console.log('driver removed from nearby drivers node'))
            .catch(er => console.log('error removing driver location info under clients nearby_drivers node: ', er));
    });
    // SET THE SOLO DRIVER GEOLOCATION INFORMATION UNDER NEARBY DRIVERS NODE FOR THE CLIENT
    geofireQuerySoloDriver.on('key_moved', (key, location, distance) => {
        console.log("NEW HIT => DRIVER M0VED: ", key);
        admin.database().ref(`clients/nearby_drivers/${client_key}/${key}`).set({
            type: 'solo',
            distance: distance,
            location: location
        })
            .then(() => console.log('driver location info sent under clients_nearby_drivers node'))
            .catch(er => console.log('error sending driver locatio info under clients nearby_drivers node: ', er));
    });
    // SET THE BOKKO DRIVER GEOLOCATION INFORMATION UNDER NEARBY DRIVERS NODE FOR THE CLIENT
    geofireQueryBokkoDriver = geofireBokkoDrivers.query({
        center: [client_geo_data.l[0], client_geo_data.l[1]],
        radius: 50
    });
    geofireQueryBokkoDriver.on('key_entered', (key, location, distance) => {
        console.log("NEW HIT => DRIVER FOUND: ", key);
        admin.database().ref(`clients/nearby_drivers/${client_key}/${key}`).set({
            type: 'bokko',
            distance: distance,
            location: location
        })
            .then(() => console.log('driver location info sent under clients_nearby_drivers node'))
            .catch(er => console.log('error sending driver locatio info under clients nearby_drivers node: ', er));
    });
    // SET THE BOKKO DRIVER GEOLOCATION INFORMATION UNDER NEARBY DRIVERS NODE FOR THE CLIENT
    // WHEN THE DRIVER MOVES
    geofireQueryBokkoDriver.on('key_moved', (key, location, distance) => {
        console.log("NEW HIT => DRIVER MOVED: ", key);
        admin.database().ref(`clients/nearby_drivers/${client_key}/${key}`).set({
            type: 'bokko',
            distance: distance,
            location: location
        })
            .then(() => console.log('driver location info sent under clients_nearby_drivers node'))
            .catch(er => console.log('error sending driver locatio info under clients nearby_drivers node: ', er));
    });
    geofireQueryBokkoDriver.on('key_exited', (key, location, distance) => {
        console.log("NEW HIT => DRIVER EXITED: ", key, location, distance);
        admin.database().ref(`clients/nearby_drivers/${client_key}/${key}`)
            .remove()
            .then(() => console.log('driver removed from nearby drivers node'))
            .catch(er => console.log('error removing driver location info under clients nearby_drivers node: ', er));
    });
});
/*******************************************************************************
                         WHEN THE CLIENT LEAVES THE APP
                              
 *******************************************************************************/
exports.whenClientLeaves = functions.database.ref('clients/geolocations/{client_uid}')
    .onDelete((snapshot) => {
    const client_key = snapshot.key;
    admin.database().ref(`clients/nearby_drivers/${client_key}`)
        .remove()
        .then(() => console.log('NEARBY_DRIVERS REMOVED FROM FIREBASE', client_key))
        .catch(er => console.log("ERROR REMOVING NEARBY DRIVERS FROM FIREBASE: ", er));
});
/*******************************************************************************
                         PAYING TICKET RESERVATION
                              
 *******************************************************************************/
exports.payingTicketReservation = functions.database.ref('payments/requests/{clientUid}/')
    .onWrite(async (snapshot) => {
    const rsv = snapshot.after.val();
    const client_uid = snapshot.after.key;
    let custom_field;
    rsv.custom_field !== null ? custom_field = rsv.custom_field : custom_field = client_uid;
    const params = {
        item_name: rsv.item_name,
        item_price: rsv.item_price,
        currency: "XOF",
        ref_command: rsv.ref_command,
        command_name: "Payment Taxibokko via PayExpress",
        env: "test",
        ipn_url: `https://us-central1-taxibokko-255214.cloudfunctions.net/ipnPayexpresse`,
        custom_field: custom_field,
    };
    const headers = {
        Accept: "application/json",
        'Content-Type': "application/json",
        API_KEY: MY_PAYEXPRESSE_API_KEY,
        API_SECRET: MY_PAYEXPRESSE_API_SECRET,
    };
    await node_fetch_1.default(payExpressUrl, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: headers
    })
        .then(async function (response) {
        console.log('RESPONSE OF THE PAY EXPRESS API: ', response);
        await response.json().then((json) => {
            console.log('JSON RETRIEVED FROM RESPONSE: ', json);
            admin.database().ref(`payments/responses/${client_uid}`).set(json)
                .then(() => console.log('payment response set'))
                .catch(er => console.log("payment request error: ", er));
        }).catch(jsonError => console.log('ERROR GETTING THE JSON OBJECT: ', jsonError));
    }).catch((err) => {
        console.log('ERROR RESPONSE OF THE PAY EXPRESS API: ', err);
        admin.database().ref(`payments/responses/${client_uid}`).set(err)
            .then(() => console.log('payment response set'))
            .catch(er => console.log("payment request error: ", er));
    });
});
/*******************************************************************************
                              IPN PAYEXPRESSE

 *******************************************************************************/
exports.ipnPayexpresse = functions.https.onRequest(async (request, response) => {
    const bod = request.body;
    console.log('body of payment retrieved from payexpresse: ', bod);
    const custom_field = bod.custom_field.split(',');
    const client_uid = custom_field[0];
    let client_first_name = '';
    let driver_uid = '';
    let client_last_name = '';
    custom_field[1] ? client_first_name = custom_field[1] : null;
    custom_field[2] ? client_last_name = custom_field[2] : null;
    custom_field[3] ? driver_uid = custom_field[3] : null;
    console.log('CLIENT UID: ', client_uid);
    console.log('DRIVER UID: ', driver_uid);
    console.log("CUSTOM FIELD RETRIEVED FROM IPN: ", custom_field);
    const ipn = {
        type_event: bod.type_event,
        ref_command: bod.ref_command,
        item_name: bod.item_name,
        item_price: bod.item_price,
        currency: bod.currency,
        command_name: bod.command_name,
        env: bod.env,
        payment_method: bod.payment_method,
        token: bod.token,
        client_uid: client_uid,
        first_name: client_first_name,
        last_name: client_last_name,
        custom_field: custom_field,
        api_key_sha256: bod.api_key_sha256,
        api_secret_sha256: bod.api_secret_sha256,
    };
    const decryptedApiKey = crypto.createHash('sha256').update(MY_PAYEXPRESSE_API_KEY).digest('hex');
    const decryptedApiSecret = crypto.createHash('sha256').update(MY_PAYEXPRESSE_API_SECRET).digest('hex');
    if (decryptedApiKey === ipn.api_key_sha256 && decryptedApiSecret === ipn.api_secret_sha256) {
        response.sendStatus(200);
        console.log('NOTIFICATION CAME FROM PAYEXPRESSE');
        //================  STORE PAYMENT INFO UNDER PAYMENTS RESPONSES TO NOTIFY CLIENT =======================
        await admin.database().ref(`payments/responses/${ipn.client_uid}/ipn`)
            .set(ipn)
            .then(() => console.log("PAY EXPRESSE IPN SENT TO FIREBASE REALTINE DATABASE", ipn))
            .catch(ipnErr => console.log('ERROR SENDING THE PAYEXPRESSE IPN TO FIREBASE REALTIME DB: ', ipnErr));
        if (ipn.client_uid == '') {
            return;
        }
        // ==========================  STORE PAYMENT INFO TO FIREBASE UNDER STATS =================================
        await admin.database().ref(`stats/payments/${ipn.client_uid}/ipn`)
            .set(ipn)
            .then(() => console.log("PAY EXPRESSE IPN SENT TO FIREBASE REALTINE DATABASE", ipn))
            .catch(ipnErr => console.log('ERROR SENDING THE PAYEXPRESSE IPN TO FIREBASE REALTIME DB: ', ipnErr));
        // ==========================SEND NOTIFICATION TO FIREBASE FOR DRIVER =================================
        if (driver_uid !== '') {
            admin.database().ref(`drivers/status/${driver_uid}/payment_status`)
                .set({ client_uid: client_uid, amount: ipn.item_price, first_name: client_first_name, last_name: client_last_name, payment_type: ipn.payment_method })
                .then(() => console.log('CLIENT PAID THE DRIVER AND NOTTFIED THE DRIVER'))
                .catch(er => console.log('ERROR NOTIFYING THE DRIVER OF THE CLIENT PAYMENT: ', er));
        }
    }
    else {
        console.log("THE API KEY AND SECRET DO NOT MATCH WITH THE ONES IN THE REQUEST");
        response.sendStatus(403);
    }
});
//# sourceMappingURL=index.js.map