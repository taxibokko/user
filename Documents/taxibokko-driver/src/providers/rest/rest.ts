import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// ---------------------------------------------DECORATOR-------------------------------------------------------
@Injectable()
export class RestProvider {

  constructor(public http: HttpClient) {
  }

// -----------------------------------------POSTDRIVER METHOD-----------------------------------------------
  postDriverInfo(driver:Object){

    // var link = "https//link-to-the-back-end";

    // var headers = new Headers(); // declaring the header to use for the http post request
    // headers.append('Content-Type','application/json'); // setting the header fo the http request
    // var requestOptions = new requestOptions({headers:headers});

    // let postData = this.http.post(link, driver, requestOptions);

    // postData.subscribe(
    //   data=> { console.log(data['_body'])},
    //   error => { console.log(error)}
    //   );

  //TEST THE METHOD WITH THE CONSOLE
    console.log(driver);

  }
}
