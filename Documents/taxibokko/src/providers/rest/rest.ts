import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DriverEntity } from '../../entities/driverEntities';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {


  constructor(public http: HttpClient) {
    // console.log('Hello RestProvider Provider');
  }

  //create a method that post the credentials of the user

  postDriverInfo(driver:DriverEntity){
    var link = "www.uniform.resources.identifiers.orlocator.com";

    let postDriver = this.http.post(link,driver);

    postDriver.subscribe();

  }

}
