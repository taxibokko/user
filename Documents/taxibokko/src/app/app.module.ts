import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { global } from '../providers/global';
import { Information } from '../pages/information/information';
import { RestProvider } from '../providers/rest/rest';
import { HttpClientModule } from '@angular/common/http';


var config = {
  backButtonText: '',
  backButtonIcon: 'md-arrow-back',
  pageTransition: 'md',
  mode:'md',
};

@NgModule({
  declarations: [
    MyApp,
    Information
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,config),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Information
  ],
  providers: [
    MyApp,
    Information,
    StatusBar,
    SplashScreen,
    Keyboard,
    global,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider
  ]
})
export class AppModule {}
