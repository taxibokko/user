import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DriverEntity} from '../../entities/driverEntities';
import $ from "jquery";
import 'intl-tel-input';
import { Information } from '../information/information';
import { RestProvider } from '../../providers/rest/rest';



//--------------------------------------------------DECORATOR----------------------------------------------------------//
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

//--------------------------------------------------CLASS REGISTER----------------------------------------------------------//
export class Register {
  	
  confirmPassword: string;
  driverEntity = new DriverEntity();
  informationPage: any =  Information;


//--------------------------------------------------CONSTRUCTOR----------------------------------------------------------//
  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider) {
    let emptyField: string = "";

    //initaliaze the fiels of the driver object {First Name, Last Name, Phone, Password}
    this.driverEntity.firstName = emptyField ;
    this.driverEntity.lastName = emptyField;
    this.driverEntity.phone = null;
    this.driverEntity.password = emptyField;

  }

//--------------------------------------------------BACK BUTTON FUNCTION----------------------------------------------------------// 
 backButtonClick(){
    this.navCtrl.pop();
  }


//--------------------------------------------------NG ONINIT FUNCTION----------------------------------------------------------//
  ngOnInit(): any {
    let telInput = $("#phone");

    telInput.intlTelInput();
    // listen to "keyup", but also "change" to update when the user selects a country
    telInput.on("keyup change", function() {
      // var intlNumber = telInput.intlTelInput("getNumber");
    });
  }


//--------------------------------------------------POST THE CREDENTIALS FUNCTION----------------------------------------------------------//
  postDriverCredentials(){

    //TESTINT THE FUNCTION
     console.log(this.driverEntity.firstName + ' ' 
              + this.driverEntity.lastName
              + ' '
              + this.driverEntity.phone
              + ' '
              + this.driverEntity.password);


     if(this.confirmPassword == this.driverEntity.password){ //todo check if the password follows certain rules
    // call the restProvider t post the credentials of the driver
       this.restProvider.postDriverInfo(this.driverEntity);

    // go to the information page to allow the driver to enter his driver's license number
      this.navCtrl.push(this.informationPage);
     }else{
       console.log('The passwords you entered do not match');
     }
     
  }

}