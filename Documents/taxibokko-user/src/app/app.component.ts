import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';


//--------------------------------------------Firebase object Configuration-----------------------------------------------
var firebaseConfig = {
  apiKey: 'AIzaSyC1u-qjosXUSZY5EWPz-jmab2T46Yf7Y2s',
  authDomain: "taxibokko.firebaseapp.com",
  databaseURL: "https://taxibokko.firebaseio.com",
  projectId: 'taxibokko',
  storageBucket: "taxibokko.appspot.com",
  messagingSenderId: "2693849356",
  appId: "1:2693849356:web:5351bf8a865d0efa"
};

//--------------------------------------------------------DECORATOR-----------------------------------------------
@Component({
  templateUrl: 'app.html'
})

//--------------------------------------------------------MY APP CLASS----------------------------------------------
export class MyApp {
//--------------------------------------------------------DOM PROPERTY----------------------------------------------
  @ViewChild(Nav) nav: Nav;
//--------------------------------------------------------PROPERTIES-----------------------------------------------
  rootPage: any = 'Login';
  pages: Array<{title: string, component: any}>;

  //--------------------------------------------------------CONSTRUCTOR-----------------------------------------------
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {

    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'Home' },
      { title: 'List', component: 'Login' }
    ];
    
    firebase.initializeApp(firebaseConfig); // Initialize firebase

  }

//--------------------------------------------------------INITIALIZE APP METHOD-----------------------------------------------
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    
  }
//--------------------------------------------------------OPEN PAGE METHOD-----------------------------------------------

  openPage(page: any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
