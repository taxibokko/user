import { Component, ViewChild, ElementRef ,NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from '../../app/app.component';
import * as firebase from 'firebase';
import * as GeoFire from 'geofire';
import { Device } from '@ionic-native/device';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// ---------------------------------------------constants declaration-----------------------------------------
declare var google: any;
const clientDbRef: string = 'Clients_geolocations/';
const driverDbRef: string = 'Drivers_geolocations/';

// ------------------------------------------------DECORATORS-------------------------------------------------
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})


// ------------------------------------------------HOME CLASS-------------------------------------------------
export class Home {

  // ------------------------------------------------HOME properties-------------------------------------------------
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  start = 'Dakar Plateau';
  end = 'Foire CICES';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  googleAutocomplete = new google.maps.places.AutocompleteService();
  autocompleteItems = [];
  autocomplete = { start: '', end: '' };
  // autocomplete1 = { input: '' };
  geocoder = new google.maps.Geocoder;
  markers= [];
  geofireClient = new GeoFire(firebase.database().ref(clientDbRef));
  geofireDriver = new GeoFire(firebase.database().ref(driverDbRef));
  hits = new BehaviorSubject([]);

  AcceptTrip = false;
  showTrip = false;

  // ------------------------------------------------CONSTRUCTOR-------------------------------------------------
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController ,
    public ngZone: NgZone, 
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public device: Device,
    public geo: Geolocation, // navigator object to get the current position of the user and track his position
    public _myApp:MyApp) {
      this.googleAutocomplete;
      this.autocompleteItems;
      this.geocoder = new google.maps.Geocoder;
      this.markers = [];
    }

// ------------------------------------------------IONVIEWDIDLOAD-------------------------------------------------
ionViewDidLoad() {
  this.findMyposition();
}



  // ------------------------------------------------SearchResultsDepart()-------------------------------------------------
  SearchResultsDepart(){
    if (this.autocomplete.start == '') {
      this.autocompleteItems = [];
      return;
    }
    this.googleAutocomplete.getPlacePredictions({ input: this.autocomplete.start },
      (predictions: { forEach: (arg0: (prediction: any) => void) => void; }, status: any) => {
        this.autocompleteItems = [];
        this.ngZone.run(() => {
          predictions.forEach((prediction: any) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

    // ------------------------------------------------SearchResultsDest()-------------------------------------------------
  SearchResultsDest(){
    if (this.autocomplete.end == '') {
      this.autocompleteItems = [];
      return;
    }
    this.googleAutocomplete.getPlacePredictions({ input: this.autocomplete.end },
      (predictions: { forEach: (arg0: (prediction: any) => void) => void; }, status: any) => {
        this.autocompleteItems = [];
        this.ngZone.run(() => {
          predictions.forEach((prediction: any) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }
  

  // ------------------------------------------------googleAutocomplete-------------------------------------------------


  // ------------------------------------------------selectSearchResult-------------------------------------------------
  selectSearchResult(item: { place_id: any; }){
  this.deleteMarkers();
  this.autocompleteItems = [];

  this.geocoder.geocode({'placeId': item.place_id}, (results: { geometry: { location: any; }; }[], status: string) => {
    if(status === 'OK' && results[0]){
      let position = {
          lat: results[0].geometry.location.lat,
          lng: results[0].geometry.location.lng
      };
      let marker = new google.maps.Marker({
        position: results[0].geometry.location,
        map: this.map,
      });
      this.markers.push(marker);
      this.map.setCenter(results[0].geometry.location);
    }
  })
}
// ------------------------------------------------iniMap()-------------------------------------------------
// initializes the google map
initMap() {
//   this.map = new google.maps.Map(this.mapElement.nativeElement, {
//     zoom: 12,
//     // center: { lat: 41.85, lng: -87.65 } //coordinates of Chicago IL
//     center: { lat: 14.74, lng: -17.47 } // coordinates of digitalis SN Ouest Foire
//   });
//   this.directionsDisplay.setMap(this.map);
// this.map = new google.maps.Map(this.mapElement.nativeElement, {
//   zoom: 7,
//   center: {lat: 41.85, lng: -87.65}
// });
  this.findMyposition();
  // this.directionsDisplay.setMap(this.map);
  
}



// ------------------------------------------------calculateAndDisplayRoute()-------------------------------------------------
calculateAndDisplayRoute() {

  this.showTrip = !this.showTrip;
  this.deleteMarkers();
  this.directionsService.route({
    origin: this.autocomplete.start,
    destination: this.autocomplete.end,
    travelMode: 'DRIVING'
  }, (response: any, status: string) => {
    if (status == 'OK') {
      this.directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

// ------------------------------------------------findMyPosition()-------------------------------------------------
//find the current position of the user and display it on the map
findMyposition(){
  this.geo.getCurrentPosition({maximumAge: 3000, timeout: 5000, enableHighAccuracy: false})
  .then((response) => {
    let myposition = new google.maps.LatLng(response.coords.latitude, response.coords.longitude);
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 15,
      zoomControl: false, // remove the plus and minus sign for zoom control
      streetViewControl: false, // remove the person icon for street view
      mapTypeControl: false, // remove the plan and satellite options for the map type
      center: myposition // center the map to the current position found
    })
    this.directionsDisplay.setMap(this.map); // display the map
  });

  let watchMyPosition = this.geo.watchPosition();
  watchMyPosition.subscribe( (dataReceived) => {

  //   // delete markers
  this.deleteMarkers();
  //   //update location
  let updatedPosition = new google.maps.LatLng(dataReceived.coords.latitude, dataReceived.coords.longitude);
  //update the current location into firebase Database
  // this.upDateClientLocation(this.device.uuid.toString(), dataReceived.coords.latitude, dataReceived.coords.longitude);
  this.upDateClientLocation('thisUser19', dataReceived.coords.latitude, dataReceived.coords.longitude);
  //call the image of the marker
    let markerIcon = 'assets/img/mapMaker.png';
  //   // add the marker
  this.addMarkers(updatedPosition, markerIcon);
  
  //add markers to show the drivers of the map
  // this.getAllDrivers(100000,[dataReceived.coords.latitude, dataReceived.coords.longitude]);
  // set the map
  this.setMarkersOnMap(this.map);
  });

}

// ------------------------------------------------addMarkers()-------------------------------------------------
addMarkers(location: any, image: any){
  let newMarker = new google.maps.Marker({
    position: location,
    icon: image,
    map: this.map
  });
  this.markers.push(newMarker);
}
// ------------------------------------------------deleteMarkers()-------------------------------------------------
deleteMarkers(){
  this.markers = [];
  this.setMarkersOnMap(null);
}

// ------------------------------------------------setMarkersOnMap()-------------------------------------------------
setMarkersOnMap(map: any){
  for( var i = 0; i < this.markers.length; i++){
    this.markers[i].setMap(map);
  }
}

// ------------------------------------------------upDateClientLocation()-------------------------------------------------
//update the current location of the client to firebase Database

upDateClientLocation(uuid: any, latd:any, longtd: any){

  this.geofireClient.set(uuid, [latd, longtd])
  .then( (_: any) => console.log('Location Updated'))
  .catch((error: any) => console.log(error));

  // if(localStorage.getItem('myKey')){ // if the item exists in the database
  //   //call the firebase database reference and store the new location
  //   firebase.database().ref(clientDbRef + localStorage.getItem('myKey')).set({
  //     uuid: uuid,
  //     latitude: latd,
  //     longitude: longtd
  //   });

  // }else { // if the item does not exist
  //   let newData = firebase.database().ref(clientDbRef).push(); // get a new key for the item
  //   newData.set({
  //     uuid: uuid,
  //     latitude: latd,
  //     longitude: longtd
  //   });
  //   localStorage.setItem('myKey', newData.key);
  // }
}

// ------------------------------------------------getAllDrivers()-------------------------------------------------
getAllDrivers(rad: number, clientLocation: number[]){

  this.geofireDriver.query({// call the geofire Object to queryt the firebase database
    center: clientLocation, // from the location of the client
    radius: rad // within this radius
  }).on('key_entered', (key: any, location: any, distance: any) => { // when a key is found
    let hit = { // store the key, location and distance to a "hit" object
      id: key,
      location: location,
      distance: distance
    }

    let currentHits = this.hits.value;
    currentHits.push(hit);
    this.hits.next(currentHits);
  }
  );
  this.setDriverMarkers(); // set the drivers markers on the map
}

// ------------------------------------------------setDriverMarkers()-------------------------------------------------
setDriverMarkers(){
  this.hits.subscribe(hits => { // subscribe to the behaviorSubject hits
    hits.forEach(element => {
      let driverPosition = new google.maps.LatLng(element.location[0], element.location[1]);
      this.addMarkers(driverPosition,'assets/img/pin.png');
    });
  });
}

// ------------------------------------------------activeTrip()-------------------------------------------------
//show details of trip
    activeTrip(){
      this.showTrip = !this.showTrip;
    }


// ------------------------------------------------presentDestinationModal()-------------------------------------------------
//present destination trip
    presentDestinationModal() {
      let DestinationModal = this.modalCtrl.create('DestinationModal', { userId: 8675309 });
      DestinationModal.present();
    }

// ------------------------------------------------presentMessageModal()-------------------------------------------------
    //present message
    presentMessageModal() {
      let MessageModal = this.modalCtrl.create('MessageModal');
      MessageModal.present();
    }

// ------------------------------------------------cancelTrip()-------------------------------------------------
    // cancle trip
  cancelAlert() {
    let alert = this.alertCtrl.create({
      subTitle: 'Êtes vous sûr(e) de vouloir annuler cette réservation?',
      buttons: ['Non' , 'Oui']
    });
    alert.present();
  }

}


